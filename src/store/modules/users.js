import { login } from "@/api/user";
import { setToken } from "@/utils/cookies";
export default{
    namespaced:true,
    state:{

    },
    mutation:{

    },
    actions:{
        /*userLogin(Context,user){
            return new Promise(resolve=>{
                login(user.email, user.password).then(res => {
                    console.log(res)
                    setToken(res.access_token)
                    this.$router.push("/");
                  })
            })
            
        }*/
        async userLogin(Context,user){
            try
            {
                const res = await login(user.email,user.password)
                setToken(res.access_token)
            }
            catch(error)
            {

            }
            
        }
    }
}