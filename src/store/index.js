import Vue from 'vue'
import Vuex from 'vuex'
import tracks from './modules/tracks'
import users from './modules/users'

Vue.use(Vuex)
//se creo un archivo llamado tracks para copiar y pegar lo que se encontraba dentro
//ya que como puede crecer,entonces se creo una carpeta modules dentro de store
export default new Vuex.Store({
  getters:{
    tracks:state=>state.tracks.tracks//esto se puede dejar pero si se usa el namespaced entonces ya no es necesario poner esto para disponer su acceso
  },
  modules: {
    tracks,
    users
  }
})
